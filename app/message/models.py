from django.db import models


class Message(models.Model):
    datetime_of_dispatch = models.DateTimeField(
        auto_now_add=True, verbose_name="Дата и время отправки"
    )
    status = models.CharField(
        max_length=20, verbose_name="Статус отправки", blank=False, null=False
    )
    mailing_id = models.ForeignKey(
        "mailing_list.MailingList",
        on_delete=models.CASCADE,
        blank=False,
        null=False
    )
    client = models.ForeignKey(
        "client.Client",
        on_delete=models.CASCADE,
        blank=False,
        null=False
    )


