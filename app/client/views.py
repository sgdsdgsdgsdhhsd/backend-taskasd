from .models import (
    Client
)
from .serializers import (
    ClientSerializer
)
from rest_framework.viewsets import (
    GenericViewSet
)
from rest_framework.mixins import (
    UpdateModelMixin,
    CreateModelMixin,
    DestroyModelMixin
)


class ClientViewSet(CreateModelMixin,
                    UpdateModelMixin,
                    DestroyModelMixin,
                    GenericViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
