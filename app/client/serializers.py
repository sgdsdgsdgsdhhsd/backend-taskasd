from .models import (
    Client
)
from rest_framework import (
    serializers
)


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ("phonenumber", "telecommunications_operator_code", "tag", "timezone")
