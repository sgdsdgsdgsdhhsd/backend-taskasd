from django.urls import include, path
from .routes import router

urlpatterns = [
    path("", include(router.urls))
]
