from django.core import validators
phonenumber_regex = validators.RegexValidator(
    regex=r'7\d{10}',
    message="Напишите номер телефона в формтае 7XXXXXXXXXX (X - цифра от 0 до 9)"
)
