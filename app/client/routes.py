from .views import ClientViewSet
from rest_framework.routers import DefaultRouter
router = DefaultRouter(trailing_slash=True)
router.register("", ClientViewSet)
