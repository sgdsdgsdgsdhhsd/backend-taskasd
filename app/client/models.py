from django.db import models
from .validators import phonenumber_regex
import pytz


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    phonenumber = models.CharField(
        max_length=11, validators=[phonenumber_regex], unique=True, verbose_name="Номер телефона", blank=False, null=False
    )
    telecommunications_operator_code = models.IntegerField(
        verbose_name="Код мобильного оператора", blank=False, null=False
    )
    tag = models.CharField(
        max_length=20, verbose_name="Тэг", blank=False, null=False
    )
    timezone = models.CharField(
        max_length=32, choices=TIMEZONES, default="UTC"
    )




