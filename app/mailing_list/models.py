import datetime
from django.db import (
    models
)


class MailingList(models.Model):

    STATUS_CHOICES = [
        ("WTS", "Ожидает начала"),
        ("IP", "В процессе"),
        ("C", "Заверешен")
    ]

    datetime_start = models.DateTimeField(
        verbose_name="Дата создания", blank=False, null=False, default=datetime.datetime.now()
    )
    datetime_end = models.DateTimeField(
        verbose_name="Дата окончания", blank=False, null=False
    )
    message_text = models.CharField(
        max_length=1000, verbose_name="Текст сообщения", blank=False, null=False
    )
    filetrs = models.JSONField(
        verbose_name="Json filed filters"
    )
    status = models.CharField(
        choices=STATUS_CHOICES, max_length=3, verbose_name="Добавил статус для лучшего ориентирования", blank=True, null=True
     )
