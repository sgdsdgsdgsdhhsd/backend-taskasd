from django.utils import timezone
from client.models import (
    Client
)
from celery.utils.log import (
    get_task_logger
)
from config.celery import (
    app
)
from .service import (
    send_sms
)
from message.models import (
    Message
)
from .models import (
    MailingList
)
logger = get_task_logger(__name__)


@app.task()
def instant_mailing(pk):
    mailing_obj = MailingList.objects.get(pk=pk)
    if mailing_obj.datetime_start < timezone.now():
        if mailing_obj.filetrs["telecommunications_operator_code"] and mailing_obj.filetrs["tag"]:
            clients = Client.objects.filter(
                tag__in=mailing_obj.filetrs["tag"], telecommunications_operator_code__in=mailing_obj.filetrs["telecommunications_operator_code"]
            )
        elif mailing_obj.filetrs["telecommunications_operator_code"]:
            clients = Client.objects.filter(
                telecommunications_operator_code__in=mailing_obj.filetrs["telecommunications_operator_code"]
            )
        elif mailing_obj.filetrs["tag"]:
            clients = Client.objects.filter(
                tag__in=mailing_obj.filetrs["tag"]
            )
        MailingList.objects.filter(
            id=mailing_obj.id
        ).update(status="IP")
        for client in clients:
            if mailing_obj.datetime_end > timezone.now():
                status = send_sms(mailing_obj.message_text, client.phonenumber)
                Message.objects.create(
                    datetime_of_dispatch=timezone.now(),
                    status=str(status),
                    mailing_id=mailing_obj,
                    client=client
                ).save()
            else:
                MailingList.objects.filter(
                    id=mailing_obj.id
                ).update(status="C")
                return True
        MailingList.objects.filter(
            id=mailing_obj.id
        ).update(status="C")
    return True


@app.task()
def delay_mailling():
    mailling_obj = MailingList.objects.filter(
        datetime_start__lte=timezone.datetime.now(), status="WTS"
    )
    if mailling_obj:
        for obj in mailling_obj:
            if obj.filetrs["telecommunications_operator_code"] and obj.filetrs["tag"]:
                clients = Client.objects.filter(
                    tag__in=obj.filetrs["tag"], telecommunications_operator_code__in=obj.filetrs["telecommunications_operator_code"]
                )
            elif obj.filetrs["telecommunications_operator_code"]:
                clients = Client.objects.filter(
                    telecommunications_operator_code__in=obj.filetrs["telecommunications_operator_code"]
                )
            elif obj.filetrs["tag"]:
                clients = Client.objects.filter(
                    tag__in=obj.filetrs["tag"]
                )
            for client in clients:
                if obj.datetime_end > timezone.now():
                    status = send_sms(obj.message_text, client.phonenumber)
                    Message.objects.create(
                        datetime_of_dispatch=timezone.datetime.now(),
                        status=str(status),
                        mailing=obj,
                        client=client
                    ).save()
                    MailingList.objects.filter(
                        id=obj.id
                    ).update(status="IP")
                else:
                    MailingList.objects.filter(
                        id=obj.id
                    ).update(status="C")
                    return True
            MailingList.objects.filter(
                id=obj.id
            ).update(status="C")
    return True
