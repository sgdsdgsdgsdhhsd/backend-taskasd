from .models import (
    MailingList
)
from .serializers import (
    MailingListSerializer
)
from rest_framework.viewsets import (
    GenericViewSet
)
from rest_framework.mixins import (
    CreateModelMixin
)


class MailingCreate(CreateModelMixin,
                    GenericViewSet):
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer
