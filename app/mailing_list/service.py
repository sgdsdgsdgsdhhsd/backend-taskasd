import requests
from config.settings import token


def send_sms(message, phonenumber):
    while True:
        headers = {
            "Token": f"{token}"
        }
        data = {
            "phone": int(phonenumber),
            "text": f"{message}"
        }
        response = requests.post(f"https://probe.fbrq.cloud/v1", headers=headers, data=data).json()
        if response['code'] == 200:
            return True
