from rest_framework import (
    serializers
)
from .models import (
    MailingList
)

from .tasks import (
    instant_mailing
)


class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = ("datetime_start", "datetime_end", "message_text", "filetrs",)

    def save(self, **kwargs):
        obj = MailingList.objects.create(
            datetime_start=self.validated_data['datetime_start'],
            message_text=self.validated_data['message_text'],
            filetrs=self.validated_data['filetrs'],
            datetime_end=self.validated_data['datetime_end'],
            status="WTS"
        )
        obj.save()
        instant_mailing.delay(obj.pk)
        return obj

