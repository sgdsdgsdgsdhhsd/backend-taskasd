from rest_framework.routers import DefaultRouter
from .views import MailingCreate
router = DefaultRouter()
router.register("create", MailingCreate)
