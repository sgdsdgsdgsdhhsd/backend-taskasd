Создайте venv в главном каталоге python -m venv venv и активируйте его source venv/bin/activate(Для линукс) venv/scripts/activte(Виндовс)
После активации перейдите в дирректорию app и напишите в консоль pip install -r req.txt
Для того что бы запустить проект
сделайте миграции
python manage.py makemigrations
python manage.py migrate


Создайте файл в каталоге config .env и поместите туда token={ваш токен, который вы отправили мне}
Запустите сервер
python manage.py runserver
Вся документация находится по url http://127.0.0.1:8000/docs/


Запустите Redis сервер
redis-server


Запустите Celery
celery -A config beat -l info

